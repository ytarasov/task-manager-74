package ru.t1.ytarasov.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.ytarasov.tm.api.service.IProjectDtoService;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;
import ru.t1.ytarasov.tm.marker.WebUnitCategory;
import ru.t1.ytarasov.tm.util.UserUtil;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(WebUnitCategory.class)
public class ProjectEndpointTest {

    @NotNull
    private final static String PROJECT_API_URL = "http://localhost:8080/api/projects/";

    @NotNull
    private String userId;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @NotNull
    private ProjectDto project = new ProjectDto("TEST PROJECT 1", "TEST PROJECT 1");

    @NotNull
    @Autowired
    private IProjectDtoService projectDtoService;

    private void saveMock(@NotNull final ProjectDto project) throws Exception {
        @NotNull final String url = PROJECT_API_URL + "save";
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(url)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk());
    }

    private List<ProjectDto> findAllMock() throws Exception {
        @NotNull final String existsByIdUrl = PROJECT_API_URL + "findAll";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(existsByIdUrl)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        return Arrays.asList(mapper.readValue(json, ProjectDto[].class));
    }

    private ProjectDto findByIdMock(@NotNull final String id) throws Exception {
        @NotNull final String findByIdUrl = PROJECT_API_URL + "findById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(findByIdUrl)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        if (json.isEmpty()) return null;
        return mapper.readValue(json, ProjectDto.class);
    }

    private int countMock() throws Exception {
        @NotNull final String url = PROJECT_API_URL + "count";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, Long.class).intValue();
    }

    private void clearMock() throws Exception {
        @NotNull final String url = PROJECT_API_URL + "clear";
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        project.setUserId(userId);
        saveMock(project);
    }

    @After
    public void tearDown() throws Exception {
        clearMock();
    }

    @Test
    public void findAll() throws Exception {
        @Nullable final List<ProjectDto> projects = findAllMock();
        Assert.assertNotNull(projects);
        Assert.assertFalse(projects.isEmpty());
    }

    @Test
    public void findById() throws Exception {
        @Nullable final ProjectDto foundProject = findByIdMock(project.getId());
        Assert.assertNotNull(foundProject);
        Assert.assertEquals(project.getName(), foundProject.getName());
    }

    @Test
    public void count() throws Exception {
        @Nullable final List<ProjectDto> projects = findAllMock();
        Assert.assertNotNull(projects);
        final int expectedSize = projects.size();
        final int foundSize = countMock();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void existsById() throws Exception {
        @NotNull final String url = PROJECT_API_URL + "existsById/" + project.getId();
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        Assert.assertTrue(mapper.readValue(json, Boolean.class));
    }

    @Test
    public void create() throws Exception {
        @NotNull final String url = PROJECT_API_URL + "create";
        final int expectedSize = countMock() + 1;
        mockMvc.perform(MockMvcRequestBuilders.put(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        final int foundSize = countMock();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void save() throws Exception {
        @NotNull final ProjectDto projectDto = new ProjectDto("TEST SAVE", "TEST SAVE");
        final int expectedSize = countMock() + 1;
        saveMock(projectDto);
        final int foundSize = countMock();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void deleteById() throws Exception {
        @NotNull final ProjectDto projectToDelete = new ProjectDto("TEST SAVE", "TEST SAVE");
        saveMock(projectToDelete);
        final int expectedSize = countMock() - 1;
        @NotNull final String url = PROJECT_API_URL + "deleteById/" + projectToDelete.getId();
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        final int foundSize = countMock();
        Assert.assertEquals(expectedSize, foundSize);
    }

}
