package ru.t1.ytarasov.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import ru.t1.ytarasov.tm.api.service.ITaskDtoService;
import ru.t1.ytarasov.tm.dto.model.TaskDto;
import ru.t1.ytarasov.tm.exception.entity.TaskNotFoundException;
import ru.t1.ytarasov.tm.exception.field.IdEmptyException;
import ru.t1.ytarasov.tm.exception.field.UserIdEmptyException;
import ru.t1.ytarasov.tm.marker.WebUnitCategory;
import ru.t1.ytarasov.tm.util.UserUtil;

import java.util.List;
import java.util.UUID;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(WebUnitCategory.class)
public class TaskServiceTest {

    @NotNull
    private static final String PROJECT_NAME_1 = "TEST PROJECT 1";

    @NotNull
    private static final String PROJECT_NAME_2 = "TEST PROJECT 2";

    @NotNull
    private TaskDto task1;

    @NotNull
    private TaskDto task2;

    private String userId;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private ITaskDtoService taskDtoService;

    @Before
    public void setUp() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        task1 = new TaskDto(PROJECT_NAME_1, PROJECT_NAME_1);
        task2 = new TaskDto(PROJECT_NAME_2, PROJECT_NAME_2);
        task1.setUserId(userId);
        task2.setUserId(userId);
        taskDtoService.save(task1);
        taskDtoService.save(task2);
    }

    @After
    public void tearDown() throws Exception {
        taskDtoService.deleteByUserId(userId);
    }

    @Test
    public void findByUserIdAndId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.findByUserIdAndId(null, task1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.findByUserIdAndId("", task1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> taskDtoService.findByUserIdAndId(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> taskDtoService.findByUserIdAndId(userId, ""));
        @Nullable final TaskDto foundTask = taskDtoService.findByUserIdAndId(userId, task1.getId());
        Assert.assertNotNull(foundTask);
        Assert.assertEquals(task1.getName(), foundTask.getName());
    }

    @Test
    public void findByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.findByUserId(null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.findByUserId(""));
        @Nullable final List<TaskDto> tasks = taskDtoService.findByUserId(userId);
        Assert.assertNotNull(tasks);
        Assert.assertFalse(tasks.isEmpty());
    }

    @Test
    public void countByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.countByUserId(null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.countByUserId(""));
        @Nullable final List<TaskDto> tasks = taskDtoService.findByUserId(userId);
        Assert.assertNotNull(tasks);
        final int expectedSize = tasks.size();
        final int foundSize = taskDtoService.countByUserId(userId).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void existsByIdAndUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.existsByUserIdAndId(null, task2.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.existsByUserIdAndId("", task2.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> taskDtoService.existsByUserIdAndId(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> taskDtoService.existsByUserIdAndId(userId, ""));
        Assert.assertTrue(taskDtoService.existsByUserIdAndId(userId, task1.getId()));
    }

    @Test
    public void deleteByUserIdAndId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> taskDtoService.deleteByUserIdAndId(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> taskDtoService.deleteByUserIdAndId(userId, ""));
        @NotNull final TaskDto taskToDelete = new TaskDto("TEST DELETE", "TEST DELETE");
        taskToDelete.setUserId(userId);
        taskDtoService.save(taskToDelete);
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.deleteByUserIdAndId(null, taskToDelete.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.deleteByUserIdAndId("", taskToDelete.getId()));
        final int expectedSize = taskDtoService.countByUserId(userId).intValue() - 1;
        taskDtoService.deleteByUserIdAndId(userId, taskToDelete.getId());
        final int foundSize = taskDtoService.countByUserId(userId).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void deleteByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.deleteByUserId(null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.deleteByUserId(""));
        @NotNull final TaskDto taskToDelete1 = new TaskDto("TEST DELETE 1", "TEST DELETE");
        @NotNull final TaskDto taskToDelete2 = new TaskDto("TEST DELETE 2", "TEST DELETE");
        @NotNull final String userIdToDelete = UUID.randomUUID().toString();
        Assert.assertNotEquals(userIdToDelete, userId);
        taskToDelete1.setUserId(userIdToDelete);
        taskToDelete2.setUserId(userIdToDelete);
        taskDtoService.save(taskToDelete1);
        taskDtoService.save(taskToDelete2);
        final int expectedSize = taskDtoService.countByUserId(userId).intValue() - 2;
        taskDtoService.deleteByUserId(userId);
        final int foundSize = taskDtoService.countByUserId(userId).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void createWithUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.createWithUserId(null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.createWithUserId(""));
        final int expectedSize = taskDtoService.countByUserId(userId).intValue() + 1;
        taskDtoService.createWithUserId(userId);
        final int foundSize = taskDtoService.countByUserId(userId).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void saveWithUserId() throws Exception {
        Assert.assertThrows(TaskNotFoundException.class, () -> taskDtoService.saveWithUserId(userId, null));
        @NotNull final TaskDto taskToSave = new TaskDto("TEST CREATE", "TEST CREATE");
        final int expectedSize = taskDtoService.countByUserId(userId).intValue() + 1;
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.saveWithUserId(null, taskToSave));
        taskDtoService.saveWithUserId(userId, taskToSave);
        final int foundSize = taskDtoService.countByUserId(userId).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

}
