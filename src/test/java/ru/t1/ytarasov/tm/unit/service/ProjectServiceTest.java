package ru.t1.ytarasov.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import ru.t1.ytarasov.tm.api.service.IProjectDtoService;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;
import ru.t1.ytarasov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ytarasov.tm.exception.field.IdEmptyException;
import ru.t1.ytarasov.tm.exception.field.UserIdEmptyException;
import ru.t1.ytarasov.tm.marker.WebUnitCategory;
import ru.t1.ytarasov.tm.util.UserUtil;

import java.util.List;
import java.util.UUID;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(WebUnitCategory.class)
public class ProjectServiceTest {

    @NotNull
    private static final String PROJECT_NAME_1 = "TEST PROJECT 1";

    @NotNull
    private static final String PROJECT_NAME_2 = "TEST PROJECT 2";

    @NotNull
    private ProjectDto project1;

    @NotNull
    private ProjectDto project2;

    private String userId;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private IProjectDtoService projectDtoService;

    @Before
    public void setUp() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        project1 = new ProjectDto(PROJECT_NAME_1, PROJECT_NAME_1);
        project2 = new ProjectDto(PROJECT_NAME_2, PROJECT_NAME_2);
        project1.setUserId(userId);
        project2.setUserId(userId);
        projectDtoService.save(project1);
        projectDtoService.save(project2);
    }

    @After
    public void tearDown() throws Exception {
        projectDtoService.deleteByUserId(userId);
    }

    @Test
    public void findByUserIdAndId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.findByUserIdAndId(null, project1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.findByUserIdAndId("", project1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> projectDtoService.findByUserIdAndId(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> projectDtoService.findByUserIdAndId(userId, ""));
        @Nullable final ProjectDto foundProject = projectDtoService.findByUserIdAndId(userId, project1.getId());
        Assert.assertNotNull(foundProject);
        Assert.assertEquals(project1.getName(), foundProject.getName());
    }

    @Test
    public void findByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.findByUserId(null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.findByUserId(""));
        @Nullable final List<ProjectDto> projects = projectDtoService.findByUserId(userId);
        Assert.assertNotNull(projects);
        Assert.assertFalse(projects.isEmpty());
    }

    @Test
    public void countByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.countByUserId(null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.countByUserId(""));
        @Nullable final List<ProjectDto> projects = projectDtoService.findByUserId(userId);
        Assert.assertNotNull(projects);
        final int expectedSize = projects.size();
        final int foundSize = projectDtoService.countByUserId(userId).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void existsByIdAndUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.existsByUserIdAndId(null, project2.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.existsByUserIdAndId("", project2.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> projectDtoService.existsByUserIdAndId(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> projectDtoService.existsByUserIdAndId(userId, ""));
        Assert.assertTrue(projectDtoService.existsByUserIdAndId(userId, project1.getId()));
    }

    @Test
    public void deleteByUserIdAndId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> projectDtoService.deleteByUserIdAndId(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> projectDtoService.deleteByUserIdAndId(userId, ""));
        @NotNull final ProjectDto projectToDelete = new ProjectDto("TEST DELETE", "TEST DELETE");
        projectToDelete.setUserId(userId);
        projectDtoService.save(projectToDelete);
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.deleteByUserIdAndId(null, projectToDelete.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.deleteByUserIdAndId("", projectToDelete.getId()));
        final int expectedSize = projectDtoService.countByUserId(userId).intValue() - 1;
        projectDtoService.deleteByUserIdAndId(userId, projectToDelete.getId());
        final int foundSize = projectDtoService.countByUserId(userId).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void deleteByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.deleteByUserId(null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.deleteByUserId(""));
        @NotNull final ProjectDto projectToDelete1 = new ProjectDto("TEST DELETE 1", "TEST DELETE");
        @NotNull final ProjectDto projectToDelete2 = new ProjectDto("TEST DELETE 2", "TEST DELETE");
        @NotNull final String userIdToDelete = UUID.randomUUID().toString();
        Assert.assertNotEquals(userIdToDelete, userId);
        projectToDelete1.setUserId(userIdToDelete);
        projectToDelete2.setUserId(userIdToDelete);
        projectDtoService.save(projectToDelete1);
        projectDtoService.save(projectToDelete2);
        final int expectedSize = projectDtoService.countByUserId(userId).intValue() - 2;
        projectDtoService.deleteByUserId(userId);
        final int foundSize = projectDtoService.countByUserId(userId).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void createWithUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.createWithUserId(null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.createWithUserId(""));
        final int expectedSize = projectDtoService.countByUserId(userId).intValue() + 1;
        projectDtoService.createWithUserId(userId);
        final int foundSize = projectDtoService.countByUserId(userId).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void saveWithUserId() throws Exception {
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectDtoService.saveWithUserId(userId, null));
        @NotNull final ProjectDto projectToSave = new ProjectDto("TEST CREATE", "TEST CREATE");
        final int expectedSize = projectDtoService.countByUserId(userId).intValue() + 1;
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.saveWithUserId(null, projectToSave));
        projectDtoService.saveWithUserId(userId, projectToSave);
        final int foundSize = projectDtoService.countByUserId(userId).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

}
