package ru.t1.ytarasov.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.api.repository.ITaskDtoRepository;
import ru.t1.ytarasov.tm.dto.model.TaskDto;
import ru.t1.ytarasov.tm.marker.WebUnitCategory;

import java.util.List;
import java.util.UUID;

@Transactional
@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(WebUnitCategory.class)
public class TaskRepositoryTest {

    @NotNull
    private static final String PROJECT_NAME_1 = "TEST PROJECT 1";

    @NotNull
    private static final String PROJECT_NAME_2 = "TEST PROJECT 2";

    @NotNull
    private TaskDto task1;

    @NotNull
    private TaskDto task2;

    @NotNull
    private static final String USER_ID = UUID.randomUUID().toString();

    @NotNull
    @Autowired
    private ITaskDtoRepository taskDtoRepository;

    @Before
    public void setUp() {
        task1 = new TaskDto(PROJECT_NAME_1, PROJECT_NAME_1);
        task2 = new TaskDto(PROJECT_NAME_2, PROJECT_NAME_2);
        task1.setUserId(USER_ID);
        task2.setUserId(USER_ID);
        taskDtoRepository.save(task1);
        taskDtoRepository.save(task2);
    }

    @After
    public void tearDown() {
        taskDtoRepository.deleteByUserId(USER_ID);
    }

    @Test
    public void findByUserIdAndId() {
        @Nullable final TaskDto foundTask = taskDtoRepository.findByUserIdAndId(USER_ID, task1.getId());
        Assert.assertNotNull(foundTask);
        Assert.assertEquals(task1.getName(), foundTask.getName());
    }

    @Test
    public void findByUserId() {
        @Nullable final List<TaskDto> tasks = taskDtoRepository.findByUserId(USER_ID);
        Assert.assertNotNull(tasks);
        Assert.assertFalse(tasks.isEmpty());
    }

    @Test
    public void countByUserId() {
        @Nullable final List<TaskDto> tasks = taskDtoRepository.findByUserId(USER_ID);
        Assert.assertNotNull(tasks);
        final int expectedSize = tasks.size();
        final int foundSize = taskDtoRepository.countByUserId(USER_ID).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void existsByIdAndUserId() {
        Assert.assertTrue(taskDtoRepository.existsByUserIdAndId(USER_ID, task1.getId()));
    }

    @Test
    public void deleteByUserIdAndId() {
        @NotNull final TaskDto taskToDelete = new TaskDto("TEST DELETE", "TEST DELETE");
        taskToDelete.setUserId(USER_ID);
        taskDtoRepository.save(taskToDelete);
        final int expectedSize = taskDtoRepository.countByUserId(USER_ID).intValue() - 1;
        taskDtoRepository.deleteByUserIdAndId(USER_ID, taskToDelete.getId());
        final int foundSize = taskDtoRepository.countByUserId(USER_ID).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void deleteByUserId() {
        @NotNull final TaskDto taskToDelete1 = new TaskDto("TEST DELETE 1", "TEST DELETE");
        @NotNull final TaskDto taskToDelete2 = new TaskDto("TEST DELETE 2", "TEST DELETE");
        @NotNull final String userIdToDelete = UUID.randomUUID().toString();
        Assert.assertNotEquals(userIdToDelete, USER_ID);
        taskToDelete1.setUserId(userIdToDelete);
        taskToDelete2.setUserId(userIdToDelete);
        taskDtoRepository.save(taskToDelete1);
        taskDtoRepository.save(taskToDelete2);
        final int expectedSize = taskDtoRepository.countByUserId(USER_ID).intValue() - 2;
        taskDtoRepository.deleteByUserId(USER_ID);
        final int foundSize = taskDtoRepository.countByUserId(USER_ID).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

}
