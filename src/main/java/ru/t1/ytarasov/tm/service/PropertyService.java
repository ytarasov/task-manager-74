package ru.t1.ytarasov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.service.IPropertyService;

@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @NotNull
    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @NotNull
    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @NotNull
    @Value("#{environment['session.key']}")
    private String sessionKey;

    @NotNull
    @Value("#{environment['session.timeout']}")
    private Integer sessionTimeout;

    @NotNull
    @Value("#{environment['database.url']}")
    private String dbUrl;

    @NotNull
    @Value("#{environment['database.username']}")
    private String dbUsername;

    @NotNull
    @Value("#{environment['database.password']}")
    private String dbPassword;

    @NotNull
    @Value("#{environment['database.driver']}")
    private String dbDriver;

    @NotNull
    @Value("#{environment['database.dialect']}")
    private String dbDialect;

    @NotNull
    @Value("#{environment['database.hbm2ddl_auto']}")
    private String dbHbm2ddlAuto;

    @NotNull
    @Value("#{environment['database.show_sql']}")
    private String showSql;

    @NotNull
    @Value("#{environment['database.format_sql']}")
    private String formatSql;

    @NotNull
    @Override
    public String getPasswordSecret() {
        return passwordSecret;
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return passwordIteration;
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return sessionKey;
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        return sessionTimeout;
    }

    @NotNull
    @Override
    public String getDbUrl() {
        return dbUrl;
    }

    @NotNull
    @Override
    public String getDbUsername() {
        return dbUsername;
    }

    @NotNull
    @Override
    public String getDbPassword() {
        return dbPassword;
    }

    @NotNull
    @Override
    public String getDbDriver() {
        return dbDriver;
    }

    @NotNull
    @Override
    public String getDbDialect() {
        return dbDialect;
    }

    @NotNull
    @Override
    public String getDbHbm2ddlAuto() {
        return dbHbm2ddlAuto;
    }

    @NotNull
    @Override
    public String getShowSql() {
        return showSql;
    }

    @NotNull
    @Override
    public String getFormatSql() {
        return formatSql;
    }

}
