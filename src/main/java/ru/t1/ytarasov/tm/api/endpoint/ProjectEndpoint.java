package ru.t1.ytarasov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@RequestMapping("/api/projects")
public interface ProjectEndpoint {

    @WebMethod
    @PutMapping("/create")
    ProjectDto create() throws Exception;

    @WebMethod
    @PostMapping("/save")
    void save(
            @WebParam(name = "project")
            @RequestBody ProjectDto project
    ) throws Exception;

    @WebMethod
    @GetMapping("/findAll")
    Collection<ProjectDto> findAll() throws Exception;

    @WebMethod
    @GetMapping("/findById/{id}")
    ProjectDto findById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    ) throws Exception;

    @WebMethod
    @GetMapping("/count")
    long count() throws Exception;

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    ) throws Exception;

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "project")
            @RequestBody ProjectDto project) throws Exception;

    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    ) throws Exception;

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll(
            @WebParam(name = "projects")
            @RequestBody List<ProjectDto> projects
    ) throws Exception;

    @WebMethod
    @DeleteMapping("/clear")
    void clear() throws Exception;

}
