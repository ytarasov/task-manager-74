package ru.t1.ytarasov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/projects")
public interface IProjectEndpoint {

    @WebMethod
    @PutMapping("/create")
    void create();

    @WebMethod
    @PostMapping("/save")
    void save(@NotNull @WebParam(name = "project") ProjectDto project) throws Exception;

    @WebMethod
    @GetMapping("/findAll")
    List<ProjectDto> findAll();

    @WebMethod
    @GetMapping("/findById/{id}")
    ProjectDto findById(@NotNull @WebParam(name = "id") final String id) throws Exception;

    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    void deleteById(@NotNull @WebParam(name = "id") final String id) throws Exception;

}
