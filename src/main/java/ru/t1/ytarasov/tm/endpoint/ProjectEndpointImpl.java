package ru.t1.ytarasov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.ytarasov.tm.api.endpoint.ProjectEndpoint;
import ru.t1.ytarasov.tm.api.service.IProjectDtoService;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;
import ru.t1.ytarasov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.t1.ytarasov.tm.api.endpoint.ProjectEndpoint")
public class ProjectEndpointImpl implements ProjectEndpoint {

    @NotNull
    @Autowired
    private IProjectDtoService projectDtoService;

    @Override
    @WebMethod
    @PutMapping("/create")
    public ProjectDto create() throws Exception {
        return projectDtoService.createWithUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public void save(
            @NotNull
            @WebParam(name = "project")
            @RequestBody
            final
            ProjectDto project
    ) throws Exception {
        projectDtoService.saveWithUserId(UserUtil.getUserId(), project);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<ProjectDto> findAll() throws Exception {
        return projectDtoService.findByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public ProjectDto findById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id")
            final
            String id
    ) throws Exception {
        return projectDtoService.findByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() throws Exception {
        return projectDtoService.countByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id")
            final
            String id
    ) throws Exception {
        return projectDtoService.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @NotNull
            @WebParam(name = "project")
            @RequestBody
            final
            ProjectDto project) throws Exception {
        projectDtoService.deleteWithUserId(UserUtil.getUserId(), project);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull @WebParam(name = "id")
            @PathVariable("id")
            final
            String id
    ) throws Exception {
        projectDtoService.deleteByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @NotNull
            @WebParam(name = "projects")
            @RequestBody
            final
            List<ProjectDto> projects
    ) throws Exception {
        projectDtoService.deleteAllWithUserId(UserUtil.getUserId(), projects);
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() throws Exception {
        projectDtoService.deleteByUserId(UserUtil.getUserId());
    }

}
